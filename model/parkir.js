var mongoose = require('mongoose');
var Schema = mongoose.Schema; 

var parkirSchema = new Schema({
    name: {type: String, required: true, max:150},
    brand: {type: String, required: true},
    tanggalMasuk:{type:Date,required: true},
    tanggalKeluar:{type:Date},
    image: {type: String, required: false},
    slot:{type:Schema.Types.ObjectId, ref: 'slot'}
})

// Export the model
module.exports = mongoose.model('parkir', parkirSchema);