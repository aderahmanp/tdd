var mongoose = require('mongoose');
var Schema = mongoose.Schema; 

var userSchema = new Schema({
    name: {type: String, required: true, max:150},
    username: {type: String, required: true},
    email:{type:String,required: true},
    password:{type:String,required: true},
    // mobil:{type:Schema.Types.ObjectId, ref: 'parkir'}
    
})

// Export the model
module.exports = mongoose.model('user', userSchema);