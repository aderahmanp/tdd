var mongoose = require('mongoose');
var Schema = mongoose.Schema; 

var slotSchema = new Schema({
    lantai: {type: String, required: true, max:150},
    block: {type: String, required: true},
    nomer:{type:String,required: true},
    mobil:{type:Schema.Types.ObjectId, ref: 'parkir'},
    isAvailable:{type:Boolean, default:true}
    
})

// Export the model
module.exports = mongoose.model('slot', slotSchema);