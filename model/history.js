var mongoose = require('mongoose');
var Schema = mongoose.Schema; 

var historySchema = new Schema({
    name: {type: String, required: true, max:150},
    brand: {type: String, required: true},
    tanggalMasuk:{type:Date,required: true},
    tanggalKeluar:{type:Date},
    biaya: {type: String, required: false}
})

module.exports = mongoose.model('history', historySchema);