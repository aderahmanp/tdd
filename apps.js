const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
var result = require('dotenv').config()
if (result.error){
    throw result.error
} else {
    console.log(result.parsed)
}

const parkir = require('./routes/parkir');
const app = express();
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL,{ useNewUrlParser: true});



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/', parkir);
app.use(logger('dev'));

app.get('/', (req, res) => {
    res.status(200).send({
        message: 'Testing'
    });
});

const PORT = process.env.PORT;

app.listen(PORT,() => {
    console.log('Server is up and running on port number' + PORT)
});