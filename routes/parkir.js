var express = require('express');
var router = express.Router();
var parkirController = require('../controller/parkir');
var multer = require('multer')
var userController = require('../controller/user')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/upload/')
    },
    filename: function (req, file,cb) {
        cb(null, Date.now() + ',jpg')
    }
})

var upload = multer({storage : storage})

router.get('hello', function(req, res) {
    res.send('yes on')
})

router.post('/kendaraanMasuk', upload.single('image'), parkirController.parkirMasuk);

router.put('/:id/kendaraanKeluar',parkirController.parkirAdmin);

router.put('/:id/updateKendaraan',parkirController.parkirUpdate);

// router.get('/:id/detailkendaraan',parkirController.parkirDetails);

router.delete('/:id/hilangkankendaraan',parkirController.parkirDelete);

// router.get('/:id/jumlahkendaraGETankeluar',parkirController.parkirKeluar);

router.get('/detail',parkirController.antrianParkir);

router.get('/biayaku/:mobilId',parkirController.parkirUser)

router.get('/slotMobil',parkirController.getSlot);

router.post('/slot',parkirController.adminSlot);

router.post('/newUser',userController.userCreate);

router.get('/historyku',parkirController.historyParkir);

module.exports = router;    