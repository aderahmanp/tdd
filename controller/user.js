const User = require('../model/user')
const bcrypt = require('bcrypt')
const saltRounds = 10;
const Ajv = require('ajv')
var userSchema = require('../schema/user')
require('dotenv').config()

exports.Test = function (req, res){
    res.json({ message: req.decoded})
}

exports.userAll = function (req, res, next) {
res.json({}).then (doc => {
    res.send({
        data: doc,
        status: 'ok'
    })
}).catch(err => {
    console.error(err)
})
}

exports.userCreate = function (req, res, next){
    let user = new User(
        {
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            
        })
var ajv = new Ajv()
// var valid = ajv.validate(userSchema, user)
var validate = ajv.compile(userSchema)
var valid = validate(user)

if (valid) {
    console.log('user data valid')
    bcrypt.hash(user.password, saltRounds).then(function(hash){
        // Store hash in your password DB
        user.password = hash
        user.save(function(err){
            if (err){
                return next(err) 
            }
            res.send('user created')

          
        }).catch(err => {
            console.error(err)
        })
    })
} else {
    console.log('user data isinvalid', validate.error)
    res.status(400)
    res.send({message: 'data invalid', error: validate.errors})
    
}
}
