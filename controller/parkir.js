const Parkir = require('../model/parkir');
const Slot=require('../model/parkirSlot');
const Data=require('../data')
const History=require('../model/history');

const fs = require('fs');

exports.test = function (req, res) {
    res.send('Greetings from the Test controller');
};

exports.parkirMasuk = function (req, res){
    var parkir = new Parkir({
            name: req.body.name,
            brand: req.body.brand,
            tanggalMasuk:new Date(),
            image: req.file ? req.file.filename: null

      });
      
      var promise = parkir.save();
      
      promise.then(function (doc) {
        Slot.fiuserCreateuserCreatend({})
        .then(function(data) {
            let slot_id=null;
            for(i=0;i<data.length;i++){
                if(data[i].isAvailable==true){
                    slot_id=data[i]._id
                    break;
                }
            }

            console.log(slot_id)
            Slot.findOneAndUpdate({_id:slot_id},{$set: {mobil:doc._id,isAvailable:false}},{new: true}, function (err, parkir){
                if (err) return nextPOST(err);
                Parkir.findOneAndUpdate({_id:doc._id},{$set: {slot:slot_id}},{new: true}, function (err, newCar){
                    if (err) return nextPOST(err);
                    res.send(newCar);
                })
                
            });
        })
        .catch(function(err){
            res.send(err)
        })
      });

};


exports.getSlot=function (req,res){
  Slot.find({}).
  populate('mobil').
  exec(function (err, mob) {
    if (err) return handleError(err);
    res.send(mob)
  });
}

// Export the model
exports.getMobil=function (req,res){
    Parkir.find({}).
    populate('slot').
    exec(function (err, slt) {
      if (err) return handleError(err);
      res.send(slt)
    });
  }
  
exports.historyParkir=function(req,res){
    History.find({})
    .then((data)=>{
        res.send(data)
    })
    .catch((error)=>{
        res.send(error)
    })
}
exports.adminSlot = function (req, res){
    var rawDocuments = Data.Binding();
   
    Slot.insertMany(rawDocuments)
    .then(function(model) {
         res.send(model)
    })
    .catch(function(err) {console.log(err);
    })
}

exports.parkirAdmin = function(req, res){
    console.log(req.params.id)
    Parkir.findOneAndUpdate({_id:req.params.id},{$set: {tanggalKeluar:new Date()}},{new: true}, function (err, parkir){

        if (err) return nextPOST(err);
        Slot.findOneAndUpdate({_id:parkir.slot},{$set: {isAvailable:true},$unset: {mobil:parkir._id}}, function (err, slot){
            if (err) return nextPOST(err);
            let nama_mobil=parkir.name
            let brand=parkir.brand
            let tanggalMasuk=parkir.tanggalMasuk.toString()
            let tanggalKeluar=new Date().toString()

            //hitung biaya waktu keluar
            let now=new Date()
                let cek_hari=parseInt(now.toString().split(' ')[2]);
                let cek_tahun=parseInt(now.toString().split(' ')[3]);
                let cek_bulan=now.toString().split(' ')[1];
                let bulan=bulan_converter(cek_bulan)
                let cek_jam=parseInt(now.toString().split(' ')[4]);
                let cek_menit=now.toString().split(' ')[4].split(':')[1];
    
                let hari_ini=cek_tahun+'/'+bulan+'/'+cek_hari+' '+cek_jam+':'+cek_menit
    
                let hari=parseInt(parkir.tanggalMasuk.toString().split(' ')[2]);
                let tahun=parseInt(parkir.tanggalMasuk.toString().split(' ')[3]);
                let bulanz=parkir.tanggalMasuk.toString().split(' ')[1];
                let bulans=bulan_converter(bulanz)
                let jam=parseInt(parkir.tanggalMasuk.toString().split(' ')[4]);
                let menit=parkir.tanggalMasuk.toString().split(' ')[4].split(':')[1];
    
                let waktu_masuk=tahun+'/'+bulans+'/'+hari+' '+jam+':'+menit
    
                console.log(waktu_masuk)
            
                var diff = Math.abs(new Date(waktu_masuk) - new Date(hari_ini));
    
                var minutes = Math.floor((diff/1000)/60);
                let hour=Math.floor(minutes/60)
                console.log(hour)
                let biaya=2000+hour*2000;
           
                var history = new History({
                    name: nama_mobil,
                    brand: brand,
                    tanggalMasuk:tanggalMasuk,
                    tanggalKeluar:tanggalKeluar,
                    biaya:biaya
        
              });
              
              var promise = history.save();
              
              promise.then(function (doc) {
                 Parkir.deleteOne({_id:parkir._id})
                 .then((doc)=>{
                    res.send('success delete')
                 })
                 .catch((err)=>{
                     res.send('error boss')
                 })
              })
              promise.catch(function (err) {
                res.send(err)
              })


           
        });
    });
};

exports.parkirUpdate = function (req, res) {
    Parkir.findByAndUpdate(req.params.id, {$set: req.body}, function (err, Parkir){
        if (err) return nextPOST(err);
        res.send('Parkiran updated');
    });
};

exports.parkirDetails = function (req, res) {
    Parkir.findById(req.params.id, function(err, parkir) {
     if (err) return next(err);
     res.send(Parkir);   
    })
}

exports.parkirDelete = (req, res) => {
    Parkir.findByIdAndRemove(req.params.id, (err, item) => {
         if (err) return (err);
         res.status(202);
         fs.unlink(`./public/upload/${item.image}`, (err) => {
             if (err) throw err;
             console.log('succesfully deleted local image');
         });
         res.send({ message : 'Deleted successfully!', userDetails: item});
    })
};

exports.parkirKeluar = function (req, res) {
    Parkir.find({name:req.params.keluar}, function (err, parkir) {
        if (err) return (err);
        res.send(parkir)
    })
};

function bulan_converter(bulan){
    if(bulan=='Jan'){
        return '01'
    }
    if(bulan=='Feb'){
        return '02'
    }
    if(bulan=='Mar'){
        return '03'
    }
    if(bulan=='Apr'){
        return '04'
    }
    if(bulan=='May'){
        return '05'
    }
    if(bulan=='Jun'){
        return '06'
    }
    if(bulan=='Jul'){
        return '07'
    }
    if(bulan=='Agu'){
        return '08'
    }
    if(bulan=='sep'){
        return '09'
    }
    if(bulan=='Oct'){
        return '10'
    }
    if(bulan=='Nov'){
        return '11'
    }
    if(bulan=="Des"){
        return '12'
    }
}

exports.parkirUser=function(req,res){
    console.log(req.params.mobilId)
    Parkir.find({_id:req.params.mobilId})
    .then(function(mob){
        console.log(mob)
        
        let now=new Date()
        let kontrol_harga=[]
        
            let result={}
            let cek_hari=parseInt(now.toString().split(' ')[2]);
            let cek_tahun=parseInt(now.toString().split(' ')[3]);
            let cek_bulan=now.toString().split(' ')[1];
            let bulan=bulan_converter(cek_bulan)
            let cek_jam=parseInt(now.toString().split(' ')[4]);
            let cek_menit=now.toString().split(' ')[4].split(':')[1];

            let hari_ini=cek_tahun+'/'+bulan+'/'+cek_hari+' '+cek_jam+':'+cek_menit

            let hari=parseInt(mob[0].tanggalMasuk.toString().split(' ')[2]);
            let tahun=parseInt(mob[0].tanggalMasuk.toString().split(' ')[3]);
            let bulanz=mob[0].tanggalMasuk.toString().split(' ')[1];
            let bulans=bulan_converter(bulanz)
            let jam=parseInt(mob[0].tanggalMasuk.toString().split(' ')[4]);
            let menit=mob[0].tanggalMasuk.toString().split(' ')[4].split(':')[1];

            let waktu_masuk=tahun+'/'+bulans+'/'+hari+' '+jam+':'+menit

            console.log(waktu_masuk)
        
            var diff = Math.abs(new Date(waktu_masuk) - new Date(hari_ini));

            var minutes = Math.floor((diff/1000)/60);
            let hour=Math.floor(minutes/60)
            console.log(hour)
            let biaya=2000+hour*2000;
            

            result.id=mob[0].id
            result.name=mob[0].name
            result.brand=mob[0].brand
            result.biaya=biaya
            result.slot='A4'
            
            kontrol_harga.push(result)
            
        
        res.json(kontrol_harga);
    })
    .catch(function(err){
        res.send('error')
    })
}
exports.antrianParkir = function(req, res) {
    console.log('semua kendaraan diparkiran');
    Parkir.find({})
    .populate('slot')
    .exec(function(err, parkir) {
        if(err) {
            res.send('Error occured')
        } else {

            let now=new Date()
            let kontrol_harga=[]
            console.log(parkir.length)
            for(var i=0;i<parkir.length;i++){
                let result={}
                let cek_hari=parseInt(now.toString().split(' ')[2]);
                let cek_tahun=parseInt(now.toString().split(' ')[3]);
                let cek_bulan=now.toString().split(' ')[1];
                let bulan=bulan_converter(cek_bulan)
                let cek_jam=parseInt(now.toString().split(' ')[4]);
                let cek_menit=now.toString().split(' ')[4].split(':')[1];

                let hari_ini=cek_tahun+'/'+bulan+'/'+cek_hari+' '+cek_jam+':'+cek_menit

                let hari=parseInt(parkir[i].tanggalMasuk.toString().split(' ')[2]);
                let tahun=parseInt(parkir[i].tanggalMasuk.toString().split(' ')[3]);
                let bulanz=parkir[i].tanggalMasuk.toString().split(' ')[1];
                let bulans=bulan_converter(bulanz)
                let jam=parseInt(parkir[i].tanggalMasuk.toString().split(' ')[4]);
                let menit=parkir[i].tanggalMasuk.toString().split(' ')[4].split(':')[1];

                let waktu_masuk=tahun+'/'+bulans+'/'+hari+' '+jam+':'+menit

                console.log(waktu_masuk)
            
                var diff = Math.abs(new Date(waktu_masuk) - new Date(hari_ini));

                var minutes = Math.floor((diff/1000)/60);
                let hour=Math.floor(minutes/60)
                console.log(hour)
                let biaya=2000+hour*2000;
                
    
                result.id=parkir[i].id
                result.name=parkir[i].name
                result.brand=parkir[i].brand
                result.biaya=biaya
                result.slot=parkir[i].slot
                
                kontrol_harga.push(result)
                
            }
            
            res.json(kontrol_harga);
        }
    });
}